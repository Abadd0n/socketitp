﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ConsoleSocketClient
{
    class Program
    {
            public static void StartClient() {  
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];  
  
            // Connect to a remote device.  
            try {  
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());  
                IPAddress ipAddress = ipHostInfo.AddressList[0];  
                IPEndPoint remoteEP = new IPEndPoint(ipAddress,11000);  
  
                // Create a TCP/IP  socket.  
                Socket sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp );  
  
                // Connect the socket to the remote endpoint. Catch any errors.  
                try {  
                    sender.Connect(remoteEP);  
  
                    //Console.WriteLine("Socket connected to {0}",  
                        //sender.RemoteEndPoint.ToString());

                    while (sender.Connected)
                    {
                        int bytesRec = sender.Receive(bytes);  
                        if(bytesRec == 0)
                        {
                            continue;
                        }
                        //Console.WriteLine("Echoed test = {0}", Encoding.ASCII.GetString(bytes,0,bytesRec));

                        var result = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        
                        var sentData = result.Split(';');
                        foreach (var d in sentData)
                        {
                            if(d.Length == 12)
                            {
                                string code = d.Substring(0, 2);
                                string value = d.Substring(2, 10);
                                Console.WriteLine("Code: {0}", code);
                                Console.WriteLine("Value: {0}", value);
                            }                            
                        }
                    }

                    // Receive the response from the remote device.  

                    // Release the socket.  
                    sender.Shutdown(SocketShutdown.Both);  
                    sender.Close();  
  
                } catch (ArgumentNullException ane) {  
                    Console.WriteLine("ArgumentNullException : {0}",ane.ToString());  
                } catch (SocketException se) {  
                    Console.WriteLine("SocketException : {0}",se.ToString());  
                } catch (Exception e) {  
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());  
                }  
  
            } catch (Exception e) {  
                Console.WriteLine( e.ToString());  
            }  
        }  
  
        public static int Main(String[] args) {  
            StartClient();  
            return 0;  
        }
    }
}
