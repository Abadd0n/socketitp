﻿using System;
using System.Net;
using System.Net.Sockets;

namespace ConsoleSocketServer
{
    class Program
    {
        public static void StartListening() {  
            
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());  
            IPAddress ipAddress = ipHostInfo.AddressList[0];  
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);  
  
            // Create a TCP/IP socket.  
            Socket listener = new Socket(ipAddress.AddressFamily,  
                SocketType.Stream, ProtocolType.Tcp );  
  
            // Bind the socket to the local endpoint and
            // listen for incoming connections.  
            try {  
                listener.Bind(localEndPoint);  
                listener.Listen(10);  

                ServerHandler serverHandler = new ServerHandler();
                Socket handler = listener.Accept();
                
                serverHandler.handler = handler;
                serverHandler.SetTimer();
                
                // Start listening for connections.  
                while (handler.Connected) 
                {  
                    
                }

                serverHandler.StopTimer();

                handler.Shutdown(SocketShutdown.Both);  
                handler.Close();

            } catch (Exception e) {  
                Console.WriteLine(e.ToString());  
            }  
  
            Console.WriteLine("\nPress ENTER to continue...");  
            Console.Read();  
  
        }  
  
        public static int Main(String[] args) {  
            StartListening();  
            return 0;  
        }

        
    }
}
