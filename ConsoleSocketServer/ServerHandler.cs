﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Timers;

namespace ConsoleSocketServer
{
    class ServerHandler
    {
        public Timer timer = new Timer(3000);
        public Random r = new Random();
        public Socket handler = null;
        public void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            var x = r.Next(1, 10);

            string cString = string.Empty;

            for (int i = 0; i < x; i++)
            {
                var a = r.Next(1, 99).ToString("D2");
                var b = RandLong(1, 9999999999, r).ToString("D10");
                cString += string.Format("{0}{1};", a, b);
            }
            Console.WriteLine(cString);
            byte[] msg = Encoding.ASCII.GetBytes(cString);  
  
            handler.Send(msg);
        }

        public void SetTimer()
        {
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Elapsed += OnTimedEvent;
        }

        public void StopTimer()
        {
            timer.Stop();
            timer.Dispose();
        }
        public long RandLong(long min, long max, Random r)
        {
            byte[] buf = new byte[8];
            r.NextBytes(buf);
            var longRandom = BitConverter.ToInt64(buf, 0);

            return Math.Abs(longRandom % (max - min) + min);
        }


    }
}
